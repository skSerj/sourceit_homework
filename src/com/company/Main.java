package com.company;

public class Main {

    public static void main(String[] args) {
        String firstPlanetName = "Mercury";
        String secondPlanetName = "Venus";
        String thirdPlanetName = "Mars";

        int numberOfPlanet = 3;

        // if (numberOfPlanet == 1) {
        //     System.out.println(firstPlanetName);
        // } else if (numberOfPlanet == 2) {
        //     System.out.println(secondPlanetName);
        // } else if (numberOfPlanet == 3) {
        //     System.out.println(thirdPlanetName);
        // } else {
        //     System.out.println("planet is wrong");
        // }
        switch (numberOfPlanet) {
            case 1:
                System.out.println("Ваша планета: " + firstPlanetName);
                break;
            case 2:
                System.out.println("Ваша планета: " + secondPlanetName);
                break;
            case 3:
                System.out.println("Ваша планета: " + thirdPlanetName);
                break;
            default:
                System.out.println("Порядковый номер указан неверно");
        }
    }
}
